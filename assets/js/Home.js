import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';

export default function Home() {


    const [heure, setHeure] = useState(new Date().getHours());
    const [minutes, setMinutes] = useState(new Date().getMinutes());
    const [sec, setSec] = useState(new Date().getSeconds());

    useEffect(() => {
        const interval = setInterval(() => {
            setHeure(new Date().getHours());
            setMinutes(new Date().getMinutes());
            setSec(new Date().getSeconds());
        }, 1000);
        return () => clearInterval(interval);
    }, [])

    return (
        <div class="d-flex justify-content-center align-items-center">
            <h2 class="h2 text-center p-2">{heure} : {minutes} : {sec}</h2>
        </div>
    )
}
ReactDOM.render(<Home />, document.getElementById('home'));