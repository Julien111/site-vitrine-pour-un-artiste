<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')           
            ->add('prenom', TextType::class, ['required' => true ])
            ->add('nom', TextType::class, [
            'required' => true,
            'constraints' => [ new NotBlank([
                        'message' => 'Entrez votre nom']), new Length(['min' => 3, 'max' => 100]), 
                    new Regex("/^[a-z ,.'-]+$/i")],
            ])
            ->add('aPropos', TextareaType::class, [
                'required' => false])            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}