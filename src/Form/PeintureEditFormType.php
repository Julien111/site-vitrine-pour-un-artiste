<?php

namespace App\Form;

use App\Entity\Peintures;
use App\Entity\Categories;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PeintureEditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, ["required" => true])
            ->add('largeur', NumberType::class, [
                "required" => true,
                'scale' => 2,                
            ])
            ->add('hauteur', NumberType::class, [
                "required" => true,
                'scale' => 2,                
            ])
            ->add('enVente', ChoiceType::class,[
                'choices'  => [                
                'Yes' => true,
                'No' => false,
                ],
            ])
            ->add('prix', MoneyType::class, [
                "required" => true,
                'scale' => 2,
                "label"=> "Le prix ",
            ])
            ->add('createdAt', DateTimeType::class, [
                'date_label' => 'Création : ',            
                "required" => true,
                'years' => range(date('Y'), date('Y') + 8)])
            ->add('description', TextareaType::class, ["required" => true, "attr" => ['id' => 'description', 'rows' => '5','cols' => '10']])
            ->add('fileImg',  FileType::class, [                
                "mapped" =>false,
                "required" => false,
                "constraints" => [
                    new Image(),                    
                ]
            ])
            ->add('categories', EntityType::class, ["class" => Categories::class, "label" => "Catégorie : ", "attr" => ["class" => "check"], 'multiple' => true, 'expanded' => true,])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Peintures::class,
        ]);
    }
}