<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
            'required' => true,
            'constraints' => [ new NotBlank([
                        'message' => 'Entrez votre nom']), new Length(['min' => 3, 'max' => 100]), 
                    new Regex("/^[a-z ,.'-]+$/i")],
            ])
            ->add('prenom', TextType::class, [
                'required' => true,
                'constraints' => [ new NotBlank([
                        'message' => 'Entrez votre prénom']), new Length(['min' => 3, 'max' => 100]), 
                    new Regex("/^[a-z ,.'-]+$/i")],
            ])
            ->add('aPropos', TextareaType::class, [
                'required' => false])
            ->add('email')
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Cochez la case pour valider votre inscription.',
                    ]),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped' => false,
                'first_options' => [
                    "label" => 'Mot de passe :'
                ],
                'second_options' => [
                    'label' => "Confirmez votre mot de passe :"
                ],
                'invalid_message' => "Les mots de passe ne sont pas similaires.",
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez un mot de passe.',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit faire {{ limit }} caractères au moins.',
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}