<?php

namespace App\Repository;

use App\Entity\Blogposts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Blogposts|null find($id, $lockMode = null, $lockVersion = null)
 * @method Blogposts|null findOneBy(array $criteria, array $orderBy = null)
 * @method Blogposts[]    findAll()
 * @method Blogposts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogpostsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Blogposts::class);
    }


    /**
     * Return blogposts by page
     * Classer les posts par date.
     * @return Event[] Returns an array of upcoming Event objects
     */
    public function findPostsByDate($page, $limit) 
    {
        return $this->createQueryBuilder('blogposts')
        ->orderBy('blogposts.createdAt', 'DESC')    
        ->setFirstResult(($page * $limit) - $limit)
        ->setMaxResults($limit)
        ->getQuery()
        ->getResult();
    }

    /**
     * Return number of posts
     * 
     * @return Event[] Returns an array of upcoming Event objects
     */
    public function getTotalPosts() 
    {
        return $this->createQueryBuilder('blogposts')
        ->select('COUNT(blogposts)')
        ->getQuery()
        ->getSingleScalarResult();
    }    

    // /**
    //  * @return Blogposts[] Returns an array of Blogposts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Blogposts
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}