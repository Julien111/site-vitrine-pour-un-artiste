<?php

namespace App\Repository;

use App\Entity\Commentaires;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commentaires|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commentaires|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commentaires[]    findAll()
 * @method Commentaires[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentairesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commentaires::class);
    }

    /**
     * Return blogposts by page
     * Classer les posts par date.
     * @return Event[] Returns an array of upcoming Event objects
     */
    public function findCommentairesByDate($page, $limit, $id) 
    {
        return $this->createQueryBuilder('c')
        ->andWhere('c.blogposts = :id')
        ->setParameter('id', $id)
        ->orderBy('c.createdAt', 'DESC')    
        ->setFirstResult(($page * $limit) - $limit)
        ->setMaxResults($limit)
        ->getQuery()
        ->getResult();
    }

    /**
     * Return peintures by page
     * Classer les peintures par date.
     * @return Event[] Returns an array of upcoming Event objects
     */
    public function findCommentairesByPeintDate($page, $limit, $id) 
    {
        return $this->createQueryBuilder('c')
        ->andWhere('c.peintures = :id')
        ->setParameter('id', $id)
        ->orderBy('c.createdAt', 'DESC')    
        ->setFirstResult(($page * $limit) - $limit)
        ->setMaxResults($limit)
        ->getQuery()
        ->getResult();
    }

    /**
     * Return number of comments by posts
     * 
     * @return Event[] Returns an array of upcoming Event objects
     */
    public function getTotalByPosts($id) 
    {
        return $this->createQueryBuilder('c')
        ->andWhere('c.blogposts = :id')
        ->setParameter('id', $id)
        ->select('COUNT(c)')
        ->getQuery()
        ->getSingleScalarResult();
    }   

    /**
     * Return number of comments by peintures
     * 
     * @return Event[] Returns an array of upcoming Event objects
     */
    public function getTotalByPeintures($id) 
    {
        return $this->createQueryBuilder('c')
        ->andWhere('c.peintures = :id')
        ->setParameter('id', $id)
        ->select('COUNT(c)')
        ->getQuery()
        ->getSingleScalarResult();
    }   
    
    // /**
    //  * @return Commentaires[] Returns an array of Commentaires objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Commentaires
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}