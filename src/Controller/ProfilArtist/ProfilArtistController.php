<?php

namespace App\Controller\ProfilArtist;

use App\Entity\User;
use App\Form\UserFormType;
use App\Form\ProfilPasswordType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @IsGranted("ROLE_ARTIST")
 * @Route("/profil")
 */
class ProfilArtistController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/profil/artist", name="artist_profil")
     */
    public function index(UserRepository $user): Response
    {
        $infoUser = $user->find($this->getUser());
        
        return $this->render('artist_profil/index.html.twig', ['user' => $infoUser]);
    }

    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/{id}/modifierProfil", name="artist_modifier_profil", methods={"GET", "POST"})
     */
    public function modifierProfil(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);    

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('artist_profil');
        }

        return $this->renderForm('artist_profil/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/{id}/modifierPassword", name="artist_modifier_password", methods={"GET", "POST"})
     */
    public function modifierPassword(Request $request, UserPasswordHasherInterface $userPasswordHasher, User $user, EntityManagerInterface $entityManager): Response
    {
        
        $form = $this->createForm(ProfilPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {            
        
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
            );
        
            $entityManager->flush();

            return $this->redirectToRoute('artist_profil');
        }
            
        return $this->render('artist_profil/pass.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}