<?php

namespace App\Controller\ProfilArtist;

use App\Entity\Peintures;
use App\Service\FileUploader;
use App\Form\PeintureFormType;
use App\Form\PeintureEditFormType;
use App\Repository\PeinturesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ARTIST")
 * @Route("/artist")
 */
class ArtistController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/", name="artist_index-painting", methods={"GET"})
     */
    public function index(PeinturesRepository $peinturesRepository): Response
    {
        return $this->render('artist/index.html.twig', [
            'peintures' => $peinturesRepository->findBy(['user' => $this->getUser()]),
        ]);
    }

    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/new", name="artist_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager, FileUploader $fileUploader): Response
    {
        $peinture = new Peintures();
        $form = $this->createForm(PeintureFormType::class, $peinture);
        $form->handleRequest($request);

        

        if ($form->isSubmitted() && $form->isValid()) {

            //ajouter la date d'aujourd'hui  à voir
            // $peinture->setCreatedAt(new \DateTime('now'));
            //ajouter l'id du user connecté à l'objet peinture
            $peinture->setUser($this->getUser());
            //ajout du slug 
            $peinture->setSlug($form->get("titre")->getData());

            //gestion de l'upload de l'image

            $objImage = $form->get('fileImg')->getData();

            if($objImage){
                $imageName = $fileUploader->upload($objImage);
                $peinture->setFileImg($imageName);
            }

            $entityManager->persist($peinture);
            $entityManager->flush();

            return $this->redirectToRoute('artist_index-painting', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('artist/new.html.twig', [
            'peinture' => $peinture,
            'form' => $form,
        ]);
    }

    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/{slug}", name="artist_show", methods={"GET"})
     */
    public function show(Peintures $peinture): Response
    {
        return $this->render('artist/show.html.twig', [
            'peinture' => $peinture,
            'slug' => $peinture->getSlug(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/{slug}/edit", name="artist_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Peintures $peinture, EntityManagerInterface $entityManager,  FileUploader $fileUploader): Response
    {
        $form = $this->createForm(PeintureEditFormType::class, $peinture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //slug
            if($form->get("titre")->getData() !== null && !empty($form->get("titre")->getData())){
                $slugger = new AsciiSlugger();
                $peinture->setSlug($slugger->slug($form->get("titre")->getData()));
            }

            //gestion de l'image
            $objImage = $form->get('fileImg')->getData();
            //nom de l'image en BDD       
            $nameImg = $peinture->getFileImg();  
            
            if($objImage !== null){

                //on supprime l'image existante
                $absoluteDir = $this->getParameter("images_directory");
                //le nom complet
                $nameTotal = $absoluteDir . "/" . $nameImg;

                if(file_exists($nameTotal)){
                    unlink($nameTotal);
                }
                //on enregistre la nouvelle images
                $imageName = $fileUploader->upload($objImage);
                $peinture->setFileImg($imageName);
            }

            $entityManager->flush();

            return $this->redirectToRoute('artist_index-painting', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('artist/edit.html.twig', [
            'peinture' => $peinture,
            'form' => $form,
            'slug' => $peinture->getSlug(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/{id}", name="artist_delete", methods={"GET","POST"})
     */
    public function delete(Request $request, Peintures $peinture, EntityManagerInterface $entityManager): Response
    {
        
        //nom de l'image en BDD       
        $nameImg = $peinture->getFileImg();
        //chemin de img
        if($nameImg){
            $absoluteDir = $this->getParameter("images_directory");
            //le nom complet
            $nameTotal = $absoluteDir . "/" . $nameImg;

            if(file_exists($nameTotal)){
                unlink($nameTotal);
            }
        }        
        
        if ($this->isCsrfTokenValid('delete'.$peinture->getId(), $request->request->get('_token'))) {
            $entityManager->remove($peinture);
            $entityManager->flush();
        }

        return $this->redirectToRoute('artist_index-painting', [], Response::HTTP_SEE_OTHER);
    }
}