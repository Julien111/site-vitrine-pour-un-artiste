<?php

namespace App\Controller\ProfilArtist;

use App\Entity\Blogposts;
use App\Form\BlogpostsType;
use App\Repository\BlogpostsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ARTIST")
 * @Route("/artist/blogposts")
 */
class ArtistBlogpostsController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/", name="artist_blogposts_index", methods={"GET"})
     */
    public function index(BlogpostsRepository $blogpostsRepository): Response
    {
        return $this->render('artist_blogposts/index.html.twig', [
            'blogposts' => $blogpostsRepository->findBy(['user' => $this->getUser()]),
        ]);
    }

    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/new", name="artist_blogposts_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $blogpost = new Blogposts();
        $form = $this->createForm(BlogpostsType::class, $blogpost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //date  
            $blogpost->setCreatedAt(new \DateTime('now'));
            //ajouter l'id du user connecté à l'objet peinture
            $blogpost->setUser($this->getUser());
            //ajout du slug 
            
            $slugger = new AsciiSlugger();
            $blogpost->setSlug($slugger->slug($form->get("titre")->getData()));

            $entityManager->persist($blogpost);
            $entityManager->flush();

            return $this->redirectToRoute('artist_blogposts_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('artist_blogposts/new.html.twig', [
            'blogpost' => $blogpost,
            'form' => $form,
        ]);
    }

    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/{slug}", name="artist_blogposts_show", methods={"GET"})
     */
    public function show(Blogposts $blogpost): Response
    {
        return $this->render('artist_blogposts/show.html.twig', [
            'blogpost' => $blogpost,
            'slug' => $blogpost->getSlug()
        ]);
    }

    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/{slug}/edit", name="artist_blogposts_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Blogposts $blogpost, EntityManagerInterface $entityManager): Response
    {

        $form = $this->createForm(BlogpostsType::class, $blogpost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($form->get("titre")->getData() !== null && !empty($form->get("titre")->getData())){
                $slugger = new AsciiSlugger();
                $blogpost->setSlug($slugger->slug($form->get("titre")->getData()));
            }
            $entityManager->flush();

            return $this->redirectToRoute('artist_blogposts_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('artist_blogposts/edit.html.twig', [
            'blogpost' => $blogpost,
            'slug' => $blogpost->getSlug(),
            'form' => $form,
        ]);
    }

    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/{id}", name="artist_blogposts_delete", methods={"POST"})
     */
    public function delete(Request $request, Blogposts $blogpost, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$blogpost->getId(), $request->request->get('_token'))) {
            $entityManager->remove($blogpost);
            $entityManager->flush();
        }

        return $this->redirectToRoute('artist_blogposts_index', [], Response::HTTP_SEE_OTHER);
    }
}