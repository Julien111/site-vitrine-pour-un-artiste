<?php

namespace App\Controller\ProfilArtist;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ARTIST")
 * @Route("/profil/accueil")
 */
class ProfilAccueilController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ARTIST")
     * @Route("/", name="profil_accueil_index", methods={"GET"})
     */
    public function index(UserRepository $user): Response
    {
        $infoUser = $user->find($this->getUser());
        return $this->render('profil_accueil/index.html.twig', ["user" => $infoUser]);
    }
}