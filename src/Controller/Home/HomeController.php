<?php

namespace App\Controller\Home;

use App\Entity\Peintures;
use App\Entity\Commentaires;
use App\Form\CommentaryType;
use App\Repository\PeinturesRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CommentairesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig', []);
    }
    
    /**
     * @Route("/about", name="app_about")
     */
    public function about(): Response
    {
        $title= "À Propos";
        return $this->render('home/about.html.twig', 
        ['title' => $title]);
    }   

    /**
     * @Route("/contact", name="app_contact")
     */
    public function contact(): Response
    {
        return $this->render('home/contact.html.twig', [
            ]);
    }

    /**
     * @Route("/peinture", name="app_peinture")
     */
    public function peinture(PeinturesRepository $peintures): Response
    {
        $peinturesElt = $peintures->findBy(array('enVente' => true));
    
        return $this->render('home/peinture.html.twig', [
            'peintures' => $peinturesElt,
            ]);
    }

    /**
     * @Route("/peinture/{slug}", name="app_show_peinture")
    */
    public function showPeinture(Peintures $peinture, Request $request, EntityManagerInterface $entityManager, CommentairesRepository $commentaireRepo): Response
    {
         //pagination des commentaires

        //on définit le nombre d'éléments par page
        $limit = 3;
        
        //on récupère le numéro de page
        $page= (int)$request->query->get('page', 1);
        
        //on récupère les posts
        $elts = $commentaireRepo->findCommentairesByPeintDate($page, $limit, $peinture->getId());

        //on a besoin du total de posts 
        
        $total = $commentaireRepo->getTotalByPeintures($peinture->getId());
        
        //formulaire pour ajouter un commentaire

        $commentaire = new Commentaires();
        $form = $this->createForm(CommentaryType::class, $commentaire);
        $form->handleRequest($request);

        //gestion du formulaire

        if ($form->isSubmitted() && $form->isValid()) {

            $commentaire->setPeintures($peinture);
            $commentaire->setCreatedAt(new \DateTime('now'));

            $entityManager->persist($commentaire);
            $entityManager->flush();

            return $this->redirectToRoute('app_show_peinture', array('slug' => $peinture->getSlug()));
        }

        return $this->render('home/showPeinture.html.twig', [
            'peinture' => $peinture,
            'form' => $form->createView(),
            'total' => $total,
            'limit' => $limit,
            'page' => $page,
            'commentaries' => $elts,
            ]);
    }
}