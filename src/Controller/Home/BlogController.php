<?php

namespace App\Controller\Home;

use DateTime;
use App\Entity\Blogposts;
use App\Entity\Commentaires;
use App\Form\CommentaryType;
use App\Repository\BlogpostsRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CommentairesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    
    /**
     * @Route("/blog", name="app_blog")
     */
    public function blog(BlogpostsRepository $blogRepository, Request $request): Response
    {

        //pagination

        //on définit le nombre d'éléments par page
        $limit = 4;
        
        //on récupère le numéro de page
        $page= (int)$request->query->get('page', 1);
        
        //on récupère les posts
        $posts = $blogRepository->findPostsByDate($page, $limit);

        //on a besoin du total de posts 
        $total = $blogRepository->getTotalPosts();
        
        $title= "My blog";
        return $this->render('blog/blog.html.twig', 
        ['title' => $title,
        'posts' => $posts,
        'total' => $total,
        'limit' => $limit,
        'page' => $page,
        ]);
    }
    
    /**
     * @Route("/blog/{slug}", name="app_show_blog")
    */
    public function showPeinture(Blogposts $blogpost, Request $request, EntityManagerInterface $entityManager, CommentairesRepository $commentaireRepo): Response
    {

        //pagination des commentaires

        //on définit le nombre d'éléments par page
        $limit = 3;
        
        //on récupère le numéro de page
        $page= (int)$request->query->get('page', 1);
        
        //on récupère les posts
        $elts = $commentaireRepo->findCommentairesByDate($page, $limit, $blogpost->getId());

        //on a besoin du total de posts 
        
        $total = $commentaireRepo->getTotalByPosts($blogpost->getId());
    
        //formulaire pour ajouter un commentaire

        $commentaire = new Commentaires();
        $form = $this->createForm(CommentaryType::class, $commentaire);
        $form->handleRequest($request);

        
        if ($form->isSubmitted() && $form->isValid()) {

            $commentaire->setBlogposts($blogpost);
            $commentaire->setCreatedAt(new \DateTime('now'));

            $entityManager->persist($commentaire);
            $entityManager->flush();

            return $this->redirectToRoute('app_show_blog', array('slug' => $blogpost->getSlug()));
        }

        
        return $this->render('blog/showBlog.html.twig', [
            'post' => $blogpost,
            'form' => $form->createView(),
            'total' => $total,
            'limit' => $limit,
            'page' => $page,
            'commentaries' => $elts,
            ]);
    }
}