<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminIndexController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/index", name="admin_index")
     */
    public function index(): Response
    {
        $title = "Espace admin";
        return $this->render('admin_index/index.html.twig', [
            'title' => $title,
        ]);
    }
}