<?php

namespace App\Entity;

use App\Repository\CommentairesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentairesRepository::class)
 */
class Commentaires
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $auteur;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Peintures::class, inversedBy="commentaires")
     */
    private $peintures;

    /**
     * @ORM\ManyToOne(targetEntity=Blogposts::class, inversedBy="commentaires")
     */
    private $blogposts;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPeintures(): ?Peintures
    {
        return $this->peintures;
    }

    public function setPeintures(?Peintures $peintures): self
    {
        $this->peintures = $peintures;

        return $this;
    }

    public function getBlogposts(): ?Blogposts
    {
        return $this->blogposts;
    }

    public function setBlogposts(?Blogposts $blogposts): self
    {
        $this->blogposts = $blogposts;

        return $this;
    }
}
