# Site vitrine artiste

Ce projet est un site vitrine qui permet à un artiste de ce présenter et de parler de son travail (de ses différentes peintures).

## Installation

## Pré-requis

- PHP 7.4
- Composer
- Symfony CLI
- Webpack encore

### Lancer le projet de développement

symfony check::requirements

### Lancer le projet de développement

- php -S localhost:8000 -t public/

### Partie connexion et inscription du

- make:auth
- make:registration-form
- Mise en place de encore pour symfony.
