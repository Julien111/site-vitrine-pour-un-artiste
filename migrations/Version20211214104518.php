<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211214104518 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_35A84671FF7747B4 ON blogposts (titre)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3AF346686C6E55B5 ON categories (nom)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3AF34668989D9B62 ON categories (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_74310B43FF7747B4 ON peintures (titre)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_74310B43989D9B62 ON peintures (slug)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_35A84671FF7747B4 ON blogposts');
        $this->addSql('DROP INDEX UNIQ_3AF346686C6E55B5 ON categories');
        $this->addSql('DROP INDEX UNIQ_3AF34668989D9B62 ON categories');
        $this->addSql('DROP INDEX UNIQ_74310B43FF7747B4 ON peintures');
        $this->addSql('DROP INDEX UNIQ_74310B43989D9B62 ON peintures');
    }
}
