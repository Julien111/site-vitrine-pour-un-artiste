<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211130144917 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE peintures_categories (peintures_id INT NOT NULL, categories_id INT NOT NULL, INDEX IDX_FFA279F2A6B72F2C (peintures_id), INDEX IDX_FFA279F2A21214B7 (categories_id), PRIMARY KEY(peintures_id, categories_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE peintures_categories ADD CONSTRAINT FK_FFA279F2A6B72F2C FOREIGN KEY (peintures_id) REFERENCES peintures (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE peintures_categories ADD CONSTRAINT FK_FFA279F2A21214B7 FOREIGN KEY (categories_id) REFERENCES categories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE peintures ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE peintures ADD CONSTRAINT FK_74310B43A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_74310B43A76ED395 ON peintures (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE peintures_categories');
        $this->addSql('ALTER TABLE peintures DROP FOREIGN KEY FK_74310B43A76ED395');
        $this->addSql('DROP INDEX IDX_74310B43A76ED395 ON peintures');
        $this->addSql('ALTER TABLE peintures DROP user_id');
    }
}
